<?php
/**
 * @file
 * uw_offer_taxonomies.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_offer_taxonomies_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'delete terms in faculty_offer_information'.
  $permissions['delete terms in faculty_offer_information'] = array(
    'name' => 'delete terms in faculty_offer_information',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in program_offer_information'.
  $permissions['delete terms in program_offer_information'] = array(
    'name' => 'delete terms in program_offer_information',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in faculty_offer_information'.
  $permissions['edit terms in faculty_offer_information'] = array(
    'name' => 'edit terms in faculty_offer_information',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in program_offer_information'.
  $permissions['edit terms in program_offer_information'] = array(
    'name' => 'edit terms in program_offer_information',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  return $permissions;
}
