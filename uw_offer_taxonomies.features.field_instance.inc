<?php
/**
 * @file
 * uw_offer_taxonomies.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function uw_offer_taxonomies_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance:
  // 'taxonomy_term-faculty_offer_information-field_faculty_blocks'.
  $field_instances['taxonomy_term-faculty_offer_information-field_faculty_blocks'] = array(
    'bundle' => 'faculty_offer_information',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'paragraphs',
        'settings' => array(
          'view_mode' => 'full',
        ),
        'type' => 'paragraphs_view',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'fences_wrapper' => 'div_div_div',
    'field_name' => 'field_faculty_blocks',
    'label' => 'Blocks',
    'required' => 0,
    'settings' => array(
      'add_mode' => 'select',
      'allowed_bundles' => array(
        'call_to_action' => -1,
        'copy_block' => 'copy_block',
        'eff_highlighted_fact' => -1,
        'sph_generic_remote_events' => -1,
        'sph_image_block' => 'sph_image_block',
        'sph_marketing_block' => 'sph_marketing_block',
        'sph_marketing_item' => -1,
        'sph_non_wcms_event' => -1,
        'sph_quicklinks_block' => 'sph_quicklinks_block',
        'sph_remote_events' => -1,
        'sph_remote_events_block' => -1,
        'uw_para_link_block' => -1,
        'uw_para_options_blocks' => -1,
        'uw_para_quicklinks' => -1,
      ),
      'bundle_weights' => array(
        'call_to_action' => 2,
        'copy_block' => 3,
        'eff_highlighted_fact' => 4,
        'sph_generic_remote_events' => 5,
        'sph_image_block' => 6,
        'sph_marketing_block' => 7,
        'sph_marketing_item' => 8,
        'sph_non_wcms_event' => 9,
        'sph_quicklinks_block' => 10,
        'sph_remote_events' => 11,
        'sph_remote_events_block' => 12,
        'uw_para_link_block' => 13,
        'uw_para_options_blocks' => 14,
        'uw_para_quicklinks' => 15,
      ),
      'default_edit_mode' => 'open',
      'entity_translation_sync' => FALSE,
      'title' => 'Paragraph',
      'title_multiple' => 'Paragraphs',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'paragraphs',
      'settings' => array(
        'label_help_description' => '',
      ),
      'type' => 'paragraphs_embed',
      'weight' => 41,
    ),
  );

  // Exported field_instance:
  // 'taxonomy_term-program_offer_information-field_program_blocks'.
  $field_instances['taxonomy_term-program_offer_information-field_program_blocks'] = array(
    'bundle' => 'program_offer_information',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'paragraphs',
        'settings' => array(
          'view_mode' => 'full',
        ),
        'type' => 'paragraphs_view',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'fences_wrapper' => 'div_div_div',
    'field_name' => 'field_program_blocks',
    'label' => 'Blocks',
    'required' => 0,
    'settings' => array(
      'add_mode' => 'select',
      'allowed_bundles' => array(
        'call_to_action' => -1,
        'copy_block' => 'copy_block',
        'eff_highlighted_fact' => -1,
        'sph_generic_remote_events' => -1,
        'sph_image_block' => 'sph_image_block',
        'sph_marketing_block' => 'sph_marketing_block',
        'sph_marketing_item' => -1,
        'sph_non_wcms_event' => -1,
        'sph_quicklinks_block' => 'sph_quicklinks_block',
        'sph_remote_events' => -1,
        'sph_remote_events_block' => -1,
        'uw_para_link_block' => -1,
        'uw_para_options_blocks' => -1,
        'uw_para_quicklinks' => -1,
      ),
      'bundle_weights' => array(
        'call_to_action' => 2,
        'copy_block' => 3,
        'eff_highlighted_fact' => 4,
        'sph_generic_remote_events' => 5,
        'sph_image_block' => 6,
        'sph_marketing_block' => 7,
        'sph_marketing_item' => 8,
        'sph_non_wcms_event' => 9,
        'sph_quicklinks_block' => 10,
        'sph_remote_events' => 11,
        'sph_remote_events_block' => 12,
        'uw_para_link_block' => 13,
        'uw_para_options_blocks' => 14,
        'uw_para_quicklinks' => 15,
      ),
      'default_edit_mode' => 'open',
      'entity_translation_sync' => FALSE,
      'title' => 'Paragraph',
      'title_multiple' => 'Paragraphs',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'paragraphs',
      'settings' => array(
        'label_help_description' => '',
      ),
      'type' => 'paragraphs_embed',
      'weight' => 41,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Blocks');

  return $field_instances;
}
