<?php
/**
 * @file
 * uw_offer_taxonomies.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function uw_offer_taxonomies_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_taxonomy_term_faculty_offer_information_pattern';
  $strongarm->value = '';
  $export['pathauto_taxonomy_term_faculty_offer_information_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_taxonomy_term_program_offer_information_pattern';
  $strongarm->value = '';
  $export['pathauto_taxonomy_term_program_offer_information_pattern'] = $strongarm;

  return $export;
}
