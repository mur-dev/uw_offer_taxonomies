<?php
/**
 * @file
 * uw_offer_taxonomies.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_offer_taxonomies_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-site-manager-vocabularies_faculty-offer-information:admin/structure/taxonomy/faculty_offer_information.
  $menu_links['menu-site-manager-vocabularies_faculty-offer-information:admin/structure/taxonomy/faculty_offer_information'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/faculty_offer_information',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Faculty offer information',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_faculty-offer-information:admin/structure/taxonomy/faculty_offer_information',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-manager-vocabularies_program-offer-information:admin/structure/taxonomy/program_offer_information.
  $menu_links['menu-site-manager-vocabularies_program-offer-information:admin/structure/taxonomy/program_offer_information'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/program_offer_information',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Program offer information',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_program-offer-information:admin/structure/taxonomy/program_offer_information',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Faculty offer information');
  t('Program offer information');

  return $menu_links;
}
